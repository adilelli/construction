const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = 3000;

// Middleware to parse JSON in the request body
app.use(bodyParser.json());

// GET endpoint
app.get('/api/data', (req, res) => {
  try {
    res.json({ message: 'GET request received' });
  } catch (error) {
    
  }
});

// POST endpoint
app.post('/api/data', (req, res) => {
  try {
    const data = req.body;
    res.json({ message: 'POST request received', data });
  } catch (error) {
    
  }
});

// Start the server
const server = app.listen(port + 1, () => {
  console.log(`Server is running on http://localhost:${port + 1}`);
});

module.exports = server;  // Export the app instance for testing