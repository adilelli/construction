// const chai = require('chai');
// const chaiHttp = require('chai-http');
// const app = require('../index');  // Import your Express app
// const expect = chai.expect;

// chai.use(chaiHttp);

// describe('GET /api/data', () => {
//   it('should return status 200 and a message', (done) => {
//     chai.request(app)
//       .get('/api/data')
//       .end((err, res) => {
//         expect(res).to.have.status(200);
//         expect(res.body).to.have.property('message').to.equal('GET request received');
//         done();
//       });
//   });
// });

// describe('POST /api/data', () => {
//   it('should return status 200 and a message with posted data', (done) => {
//     const postData = { key: 'value' };

//     chai.request(app)
//       .post('/api/data')
//       .send(postData)
//       .end((err, res) => {
//         expect(res).to.have.status(200);
//         expect(res.body).to.have.property('message').to.equal('POST request received');
//         expect(res.body).to.have.property('data').to.deep.equal(postData);
//         done();
//       });
//   });
// });

// // Shutdown server after all tests
// after((done) => {
//   app.close(() => {
//     console.log('Server shut down gracefully.');
//     done();
//   });
// });